package helloWorld;

/**
 * Just a simple piece of code, that print "Hello World!" message
 */
public class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello World!");
    }
}
